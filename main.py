from model.article import Article
# Module python permettant de créer des Dataframes
import pandas as pd


# Fonction si on veut ajouter oui ou non un nouveau article
def AjoutArticle():
    
    # Réponce de l'utilisateur : Oui/Non
    answer = input("Voulez vous ajouter un article dans votre panier ? (Oui/Non)")
    
    if answer == "Oui":
        
        # Paramettre de l'article pour le creer (nom=str,quantite=int,prix=float)
        while True:
            try:
                nom = input("Quel est le nom de l'article ?") or "Jeux_Video"
                quantite = int(input("Quel quantité vous allez prendre ?") or 3) 
                prix = float(input("Quel est son prix ?") or 17.54) 
            except ValueError:
                print("Mauvaise Valeur!")
                continue
            else:
                break 
                
        CreateArticle(nom , quantite, prix)
        
    else:
        if answer != "Non":
            print("Je ne vous ai pas compris.")
            return AjoutArticle()


# Fonction pour Créer un article
def CreateArticle(nom , quantite, prix):
    article = Article(nom, quantite, prix)
    # On ajoute article dans la liste articles 
    articles.append(article)
    
    if retry == True : AjoutArticle() 
    
#############################################################################################


print("Votre panier comporte les articles suivants :")

# Liste des articles dans le panier
articles = []

# Booléen False pour ne pas demander si on veut ajouter oui ou non un nouveau article
retry = False

# Article de base d’un panier chez Cultura en temps de confinement
CreateArticle("Nintendo_Switch", 1, 299.99)
CreateArticle("Jeu_Mario_Kart", 1, 49.99)
CreateArticle("Jeu_Ring_Fit", 1, 64.99)
CreateArticle("Livre_Python", 1, 32.40)
CreateArticle("DVD_Naruto", 1, 88.99) 
CreateArticle("Jeu_54_cartes", 2, 3.99)

# Booléen True pour demander si on veut ajouter oui ou non un nouveau article
retry = True

# Ajout de nouveau article dans le panier 
AjoutArticle()

# Ajout des colonne du DataFrame
df = pd.DataFrame(columns =["Nom", 
                    "Quantité",
                    "Prix unitaire", 
                    "Prix Total" ]) 

# index
l = 1
# Boucle de chaque articles pour les ajoutées a la 'DataFrame' df
for article in articles:
    dfadd = pd.DataFrame({"Nom":[article.get_nom()], 
                    "Quantité":[article.get_quantite()],
                    "Prix unitaire":[article.get_prix()], 
                    "Prix Total":[article.get_prix_total()]},index = [l]) 
    df = df.append(dfadd) 
    l += 1
    
print(df)

print("Il y a {} articles dans le paniers !".format(len(articles)))

# Le Prix Total des articles
prixT = 0
for prix in Article.prix:
    prixT += prix
print("Prix total de la commande : ", prixT, "euros")


# Remise de 10% sur l’ensemble de la commande en étant membre VIP de Cultura
reducVIP = round(10*prixT/100, 2)
print("Vous bénéficiez en tant que membre de VIP d'une réduction de : ", reducVIP, "euros")


# Le Prix Total des articles avec la réduction 
prixTr = round(prixT-reducVIP, 2)
print("Votre commande s'élève donc à : ", prixTr, "euros")