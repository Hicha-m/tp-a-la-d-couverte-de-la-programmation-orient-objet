class Article:
    
    # Liste des prix des articles
    prix = []
    
    # initialisation de l'article (nom de l'article, sa quantité, et son prix)
    def __init__(self, nom, quantite, prix):
        self.nom = nom
        self.quantite = quantite
        self.prix = prix
        # Ajout du prix total de l'article dans la liste 'prix'
        Article.prix.append(round(self.quantite*self.prix, 2))

    # Fonction pour récupérer son nom
    def get_nom(self):
        return self.nom
    # Fonction pour récupérer sa quantité
    def get_quantite(self):
        return self.quantite
    # Fonction pour récupérer son prix
    def get_prix(self):
        return self.prix
    # Fonction pour récupérer son prix total
    def get_prix_total(self):
        return round(self.quantite*self.prix, 2)
        

    