# TP - A la découverte de la programmation orienté objet

![alt text](https://gitlab.com/Hicha-m/tp-a-la-d-couverte-de-la-programmation-orient-objet/-/raw/master/image.jpg?raw=true)

Lien du PDF pour suivre les étapes : [pdf](https://gitlab.com/Hicha-m/tp-a-la-d-couverte-de-la-programmation-orient-objet/-/raw/master/Fiche%20Presentation%20Paradigme%20de%20Programmation.pdf)

## Consigne

***Créer une classe Panier***

avec les méthodes suivantes :

+ ajouter_article : ajoute un article au panier
+ enlever_article : enlève un article au panier (celle là peut être difficile)
+ vider : qui enlève tous les articles du panier
+ prix_total : qui calcule le prix total du panier
+ ajout_reduction : qui permet d'intégrer une réduction dans le calcul du prix (en programmation objet, on appelle ça un setter - il est bon de vérifier ce qui est ajouté)

et les attributs suivants :

+ une liste articles pour stocker les objets articles
+ un attribut reduction pour la réduction totale sur la panier.

Faire un programme exemple...
